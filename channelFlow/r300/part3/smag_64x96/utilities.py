import os
import math
import numpy as np
import matplotlib.pyplot as plt



# function to get the (x,y,z) coordinates of faceCenters
def getCoords(path2File):
    lookupBegin = '(';
    lookupEnd   = ')';
    isOn = 1;
    with open(path2File) as f:
        for num, row in enumerate(f, 1):
            if lookupEnd in row[0]:
                iEnd   = num;
            if isOn and lookupBegin in row[0]:
                iStart = num;
                isOn   = 0;
    
    # get (x,y,z) coordinates of face centers 
    data = []
    idx = 1
    with open(path2File) as file:
    	for line in file:
    		if idx >iStart and idx <iEnd:
    			temp = (line.lstrip('(').rstrip(')\n').split())
    			data.append(temp)
    		idx += 1
    
    # get row and column lengths
    rowDim = len(data);
    colDim = len(data[0]);


    # convert and transfer data to floating columns
    xCoord = np.zeros(rowDim);
    yCoord = np.zeros(rowDim);
    zCoord = np.zeros(rowDim);
    
    for i in range(rowDim):
    	xCoord[i] = float(data[i][0]);
    	yCoord[i] = float(data[i][1]);
    	zCoord[i] = float(data[i][2]);
    

    return xCoord, yCoord, zCoord, rowDim, colDim


# get (u,v,w) velocity components
def getVelocity(path2File, rowDim, colDim):
    lookupBegin = '(';
    lookupEnd   = ')';
    isOn = 1;
    with open(path2File) as myFile:
    	for num, row in enumerate(myFile, 1):
    		if lookupEnd in row[0]:
    			iEnd = num;
    		if isOn and lookupBegin in row[0]:
    			iStart   = num;
    			isOn = 0;
    
    # get 3-component velocity data 
    data = []
    idx = 1
    with open(path2File) as file:
    	for line in file:
    		if idx >iStart and idx <iEnd:
    			temp = (line.lstrip('(').rstrip(')\n').split())
    			data.append(temp)
    		idx += 1
    
    
    
    # convert and transfer data to floating columns
    Uval = np.zeros(rowDim);
    Vval = np.zeros(rowDim);
    Wval = np.zeros(rowDim);
    
    for i in range(rowDim):
    	Uval[i] = float(data[i][0]);
    	Vval[i] = float(data[i][1]);
    	Wval[i] = float(data[i][2]);
    
    return Uval, Vval, Wval
    

# get sorted files
def getSortedFiles(availFiles, totalTimeSteps):
    simTime = np.zeros(totalTimeSteps);
    for t in range(totalTimeSteps):
        simTime[t] = float(availFiles[t]);
    # sort into ascending order
    simTime.sort();

    return simTime



# get all temporal files
def getTotalTemporalVelocity(totalTimeSteps, availFiles, 
        rowDim, colDim, home, planeNbr, baseDir): 
    
    # extract temporal data
    U = np.zeros((rowDim, totalTimeSteps));
    V = np.zeros((rowDim, totalTimeSteps));
    W = np.zeros((rowDim, totalTimeSteps));
    
    # base directory
    baseFile = baseDir + '/postProcessing/surfaceSampling/'; 
    

    if planeNbr == 0:
        linkFile = '/plane0/vectorField/U';
    elif planeNbr == 1:
        linkFile = '/plane1/vectorField/U';
    elif planeNbr == 2:
        linkFile = '/plane2/vectorField/U';
    elif planeNbr == 3:
        linkFile = '/plane3/vectorField/U';
    else:
        print 'wrong input in planeNbr, func: getTotalTemporalVelocity !';
    
    for t in range(totalTimeSteps):
        
        # get rid of trailing zeros 
        if availFiles[t] - int(availFiles[t]) < 1e-5:
            currentSimTime = str(int(availFiles[t]));
        else:
            currentSimTime = str(availFiles[t]);

        updateFile = baseFile + currentSimTime + linkFile;
        
        # extract velocity data
        U[:, t], V[:, t], W[:, t] = getVelocity(updateFile, rowDim, colDim);
        # show progress
        print 'accessing file %d out of %d'%(t+1,totalTimeSteps);
    
    return U, V, W


# manage data from coordinates
def dataCoordinateManager(xCoord, yCoord, zCoord, Nx, Nz):
    
    # get all 258 unique X-coordinates and indices sorted in ascending order
    #uniqueDataListX, idxList = np.unique(xCoord, return_index=True);
    
    # XXX: debugging !!!
    #print uniqueDataListX
    uniqueTemp, null_ = np.unique(xCoord, return_index=True);
    idxList = np.where( abs(np.diff(uniqueTemp))>1e-4 );
    uniqueDataListX = uniqueTemp[idxList];
   
    uniqueDataListX = np.append(uniqueDataListX, uniqueTemp[-1]);
    idxList = np.append(idxList, len(uniqueDataListX));
    idxList = null_[idxList];
    #print len(uniqueDataListX), uniqueDataListX
    
    #uniqueTemp, null_ = np.unique(xCoord, return_index=True);
    #idxList = np.where( abs(np.diff(uniqueTemp))>1e-4 );
    #uniqueDataListX = uniqueTemp[idxList];
    #print len(uniqueDataListX)
    #uniqueUpdated = uniqueTemp[idxList];
    #print uniqueUpdated[-1], uniqueTemp[-1]
    #print len(uniqueUpdated)
    #print uniqueUpdated
    #test = np.insert(uniqueUpdated, len(uniqueUpdated), uniqueTemp[-1]);
    #testIdx = xCoord[null_[idxList]];
    #= np.insert(uniqueUpdated, len(uniqueUpdated), uniqueTemp[-1]);
    #lastIdx =  xCoord[null_[-1]];
    #idxList = np.insert(testIdx, len(testIdx), null_[-1]);
    #print idxList
    #print xCoord[idxList]
    #print xCoord[null_[idxList]]
    #print test
    #print uniqueDataListX[-1]

    # surface of coordinates (x, z)
    surfacePlaneIdxZ = np.zeros((Nx, Nz));
    
    for i in range(Nx):
        # get Nz Z-coordinate indices values per ith X-coordinate
        #repeatingIdx = np.where( xCoord == uniqueDataListX[i] );#abs(xCoord - uniqueDataListX[i])<1e-4 );
        repeatingIdx = np.where( abs(xCoord - uniqueDataListX[i])<1e-4 );

        # get Nz Z-coordinate values from indices
        temp = zCoord[repeatingIdx];
        # sort into acending order the Z-coordinates
        temp.sort();

        # convert array into 1D vector
        idxVec = np.concatenate(repeatingIdx);
        # sort ascending order and return index
        sortedIdx = np.argsort(zCoord[repeatingIdx]);
         
        # XXX: debugging !!
        #print uniqueDataListX
        #print xCoord
        #np.savetxt('test_xCoords', xCoord)
        #np.savetxt('test_uniqueData', uniqueDataListX)
        #print i
        # save Z-data indices to ith X-entry
        surfacePlaneIdxZ[i, :] = idxVec[sortedIdx]; 
        


    return idxList, surfacePlaneIdxZ 


# re-arrange data into the format: data(Nx, Nz, t) 
def arrangeData(xIdx, zIdx_perX, U, V, W, maxIter, Nx, Nz):
    Uvelocity = np.zeros((Nx, Nz, maxIter));
    Vvelocity = np.zeros((Nx, Nz, maxIter));
    Wvelocity = np.zeros((Nx, Nz, maxIter));

    for i in range(Nx):
        for k in range(Nz):
            for t in range(maxIter):
                Uvelocity[i, k, t] = U[zIdx_perX[i, k], t];
                Vvelocity[i, k, t] = V[zIdx_perX[i, k], t];
                Wvelocity[i, k, t] = W[zIdx_perX[i, k], t];


    return Uvelocity, Vvelocity, Wvelocity



# compute and return the wavenumber vector
def getWavenumber(r, Nk):
    
    kappa = np.zeros(Nk);
    L = max(r) - min(r);
    for n in range(Nk):
        kappa[n] = 2*math.pi*n/L; # (Lx/2);

    return kappa


# check if ESD is correct 
def checkSpectraResults(E, signal):
    # sum up ESD values (disregard first value)
    sumE = 0;
    for k in range(1, len(E)):
        sumE += E[k];
    
    # compute biased variance of signal
    Nsignal = len(signal);
    
    signalVAR = 0;
    signalAVG = np.mean(signal);
    
    for i in range(Nsignal):
        signalVAR += (signal[i] - signalAVG)**2;
    signalVAR = signalVAR/Nsignal;
    
    # assert the two are more-or-less equal !
    np.testing.assert_almost_equal(signalVAR, sumE);



# get spectra via fft
def getSpectra(xIdx, zIdx_perX, U, V, W, 
        maxIter, Umean, Vmean, Wmean, 
        xCoords, zCoords, Nx, Nz,
        planeNbr):
   
    # frequency of Fourier Transform
    Nkx = Nx; # streamwise
    Nkz = Nz; # spanwise

   
    # calculate fluctuations (prime values)
    UtempPrime = np.subtract(U, Umean);
    VtempPrime = np.subtract(V, Vmean); 
    WtempPrime = np.subtract(W, Wmean);

    Uprime, Vprime, Wprime = arrangeData(xIdx, zIdx_perX, 
                                UtempPrime, VtempPrime, 
                                WtempPrime, maxIter, Nx, Nz);
        

    # compute ESD and take precaution with Nyquiest theorem
    if (Nkx%2 == 0):
        # Nkx is an even number
        nxf = Nkx/2;     # Nyquist frequency
    else:
        # Nkx is an odd number
        nxf = (Nkx+1)/2; # Nyquist frequency
 
    Eu11 = np.zeros(nxf);
    Eu22 = np.zeros(nxf);
    Eu33 = np.zeros(nxf);
  
    if (Nkz%2 == 0):
        # Nkz is an even number
        nzf = Nkz/2;     # Nyquist frequency
    else:
        # Nkz is an odd number
        nzf = (Nkz+1)/2; # Nyquist frequency
 
    Ew11 = np.zeros(nzf);
    Ew22 = np.zeros(nzf);
    Ew33 = np.zeros(nzf);   
    
    for t in range(maxIter):
            
        Eu11 += getESD(Nkx, Nz, Uprime[:, :, t], Nx, Nz);
        Eu22 += getESD(Nkx, Nz, Vprime[:, :, t], Nx, Nz);
        Eu33 += getESD(Nkx, Nz, Wprime[:, :, t], Nx, Nz);
        
        Ew11 += getESD(Nkz, Nx, Uprime[:, :, t], Nx, Nz);
        Ew22 += getESD(Nkz, Nx, Vprime[:, :, t], Nx, Nz);
        Ew33 += getESD(Nkz, Nx, Wprime[:, :, t], Nx, Nz);
        
        ## testing, delete once done !
        #Utemp = np.zeros(Nx);
        #for i in range(Nx):
        #    Utemp[i] = np.mean(Uprime[i, :, t]);
        #
        #Fy_test  = np.multiply(np.fft.fft(Utemp), 1.0/Nx);
        ## average by collapsing across spanwise direction             
        #temp_test = np.multiply(Fy_test, np.conjugate(Fy_test));
        ## finalize collapse of data by dividing temp with correct N-pts 
        #temp = temp_test; #np.multiply(temp_test, 1.0/Nz);
    
        ## convert data into meaningful ESD
        #if (Nkx%2 == 0):
        #    # Nk is an even number
        #    nf = Nkx/2;     # Nyquist frequency
        #    # energy spectral density
        #    ESD  = np.zeros(nf);
        #
        #    for k in range(nf-1):
        #        ESD[k] = 2*temp[k].real;
        #    ESD[nf-1] = temp[nf-1].real;
        #else:
        #    # Nk is an odd number
        #    nf = (Nkx+1)/2; # Nyquist frequency
        #    # energy spectral density
        #    ESD  = np.zeros(nf);
        #
        #    for k in range(nf):
        #        ESD[k] = 2*temp[k].real;
        #
        #checkSpectraResults(ESD, Utemp) 
        
        # print progress every xth interval
        if (t%10 == 0):
            print 'ESD: this is the %dth iteration out of %d.'%(t,maxIter); 

    # average over time
    Eu11 = np.multiply(Eu11, 1.0/maxIter);
    Eu22 = np.multiply(Eu22, 1.0/maxIter);
    Eu33 = np.multiply(Eu33, 1.0/maxIter);
    Ew11 = np.multiply(Ew11, 1.0/maxIter);
    Ew22 = np.multiply(Ew22, 1.0/maxIter);
    Ew33 = np.multiply(Ew33, 1.0/maxIter);

    # # # # # # # # # # # # # # # # # # # # # # # #

    # wavenumber calculation
    kx = getWavenumber(xCoords[xIdx]           , nxf); 
    kz = getWavenumber(zCoords[zIdx_perX[0, :]], nzf);

   
    # XXX: testing brute force method
    #bruteForce(Uprime, Vprime, Wprime, xCoords[xIdx], zCoords[zIdx_perX[0, :]]);   
    
    
    # XXX: these are for debugging only
    # # # # # # # # # # # # # # # # # #
    #visualizeData(kx, Eu11, 1)
    #visualizeData(kx, Eu22, 1)
    #visualizeData(kx, Eu33, 1)
    #visualizeData(kz, Ew11, 1)
    #visualizeData(kz, Ew22, 1)
    #visualizeData(kz, Ew33, 1)
    # # # # # # # # # # # # # # # # # #
    saveData(kx, kz, 
             Eu11, Eu22, Eu33,
             Ew11, Ew22, Ew33, 1,
             planeNbr);


# compute ESD via FFT of velocity fluctuations
def getESD(Nk, N, data, Nx, Nz):
    # # #
    # data must be arrange in the following format:
    # data(Nx, Nz) -- i.e. velocity at (streamwise, spanwise)
    # # # # #
    Fy   = np.zeros((Nk, N), dtype=complex);
    temp = np.zeros(Nk, dtype=complex);
    
    if N == Nz: 
        # streamwise direction
        for i in range(N):
            # FT in the streamwise direction
            Fy[:, i] = np.multiply(np.fft.fft(data[:, i]), 1.0/Nx);
            # average by collapsing across spanwise direction             
            temp += np.multiply(Fy[:, i], np.conjugate(Fy[:, i]));
    else:
        # spanwise direction
        for i in range(N):
            # FT in the spanwise direction
            Fy[:, i] = np.multiply(np.fft.fft(data[i, :]), 1.0/Nz);
            # average by collapsing across streamwise direction
            temp += np.multiply(Fy[:, i], np.conjugate(Fy[:, i]));
    # finalize collapse of data by dividing temp with correct N-pts 
    temp = np.multiply(temp, 1.0/N);
    
    # convert data into meaningful ESD
    if (Nk%2 == 0):
        # Nk is an even number
        nf = Nk/2;     # Nyquist frequency
        # energy spectral density
        ESD  = np.zeros(nf);
        
        for k in range(nf-1):
            ESD[k] = 2*temp[k].real;
        ESD[nf-1] = temp[nf-1].real;
    else:
        # Nk is an odd number
        nf = (Nk+1)/2; # Nyquist frequency
        # energy spectral density
        ESD  = np.zeros(nf);
        
        for k in range(nf):
            ESD[k] = 2*temp[k].real;
        
    # NB: 
    # ESD[0] is irrelevant, it is just the mean...
    return ESD



# get mean velocity values from averages profiles
def getMeanVelocity(path2File, planeID):
    
    # all available files
    availFiles = os.listdir(path2File)
    # total time steps used in simulation
    totalFiles = len(availFiles);
    # get files names
    files = getSortedFiles(availFiles, totalFiles);
    
    # select latest file
    thisFile = max(files);

    # wall-normal planes
    yPlane0 = 0.0149994;
    yPlane1 = 0.0491272;
    yPlane2 = 0.5;
    yPlane3 = 0.98;

    # averaged files
    isReached_u = 0; 
    updateFile = path2File + str(thisFile) + '/UMean_X.xy';
    with open(updateFile) as file:
        for line in file:
            temp = line.strip().split()
            if str(yPlane0) in temp[0]:
                Umean0 = float(temp[1])
            if str(yPlane1) in temp[0]:
                Umean1 = float(temp[1])
            if isReached_u == 0 and str(yPlane2) in temp[0]:
                Umean2 = float(temp[1])
		isReached_u = 1;
            if str(yPlane3) in temp[0]:
                Umean3 = float(temp[1])
	
    isReached_v = 0;
    updateFile = path2File + str(thisFile) + '/UMean_Y.xy';
    with open(updateFile) as file:
        for line in file:
            temp = line.strip().split()
            if str(yPlane0) in temp[0]:
                Vmean0 = float(temp[1])
            if str(yPlane1) in temp[0]:
                Vmean1 = float(temp[1])
            if isReached_v == 0 and str(yPlane2) in temp[0]:
                Vmean2 = float(temp[1])
		isReached_v = 1;
            if str(yPlane3) in temp[0]:
                Vmean3 = float(temp[1])

    isReached_w = 0;
    updateFile = path2File + str(thisFile) + '/UMean_Z.xy';
    with open(updateFile) as file:
        for line in file:
            temp = line.strip().split()
            if str(yPlane0) in temp[0]:
                Wmean0 = float(temp[1])
            if str(yPlane1) in temp[0]:
                Wmean1 = float(temp[1])
            if isReached_w == 0 and str(yPlane2) in temp[0]:
                Wmean2 = float(temp[1])
	    	isReached_w = 1;
            if str(yPlane3) in temp[0]:
                Wmean3 = float(temp[1])

    # determine wall-normal plane
    if planeID == 0: 
        # plane 0
        Umean = Umean0;
        Vmean = Vmean0;
        Wmean = Wmean0;
        pass
    elif planeID == 1:
        # plane 1
        Umean = Umean1;
        Vmean = Vmean1;
        Wmean = Wmean1;
        pass
    elif planeID == 2:
        # plane 2
        Umean = Umean2;
        Vmean = Vmean2;
        Wmean = Wmean2;
        pass
    elif planeID == 3:
        # plane 3
        Umean = Umean3;
        Vmean = Vmean3;
        Wmean = Wmean3;
        pass

    return Umean, Vmean, Wmean#Umean0, Vmean0, Wmean0, Umean1, Vmean1, Wmean1, Umean2, Vmean2, Wmean2, Umean3, Vmean3, Wmean3


# write data to file
def saveData(xParam, zParam, 
             signal_uu1, signal_vv1, signal_ww1,
             signal_uu3, signal_vv3, signal_ww3, 
             isDFTspectra, planeNbr):
   
    if isDFTspectra:
        # rename params to fit
        kx = xParam; 
        kz = zParam;
        Euu1 = signal_uu1; Euu3 = signal_uu3;
        Evv1 = signal_vv1; Evv3 = signal_vv3;
        Eww1 = signal_ww1; Eww3 = signal_ww3;

        # save as file names
        fileNameX = planeNbr+'Estreamwise';
        fileNameZ = planeNbr+'Espanwise';

        # initialize file to write data in streamwise direction
        scfx = open(fileNameX, 'w');
            
        tempRx =  '% \t    1D Energy spectra in streamwise direction\n';
        tempRx += '% \t    current wall-normal plane: '+planeNbr+'\n';
        tempRx += '%     kx \t     Euu1 \t     Evv1 \t     Eww1\n';
        for i in range(len(kx)):
            tempRx += '%e \t %e \t %e \t %e\n'%(kx[i], Euu1[i], Evv1[i], Eww1[i]);
        
        # write streamwise data to file
        scfx.write(tempRx);
        scfx.close();

        # initialize file to write data in spanwise direction
        scfz = open(fileNameZ, 'w');
        
        tempRz =  '% \t    1D Energy spectra in spanwise direction\n';
        tempRz += '% \t    current wall-normal plane: '+planeNbr+'\n';
        tempRz += '%     kz \t     Euu3 \t     Evv3 \t     Eww3\n';
        for i in range(len(kz)):
            tempRz += '%e \t %e \t %e \t %e\n'%(kz[i], Euu3[i], Evv3[i], Eww3[i]);

        # write to file
        scfz.write(tempRz);
        scfz.close();
    else:
        # rename params to fit
        x = xParam; 
        z = zParam;
        Ruu1 = signal_uu1; Ruu3 = signal_uu3;
        Rvv1 = signal_vv1; Rvv3 = signal_vv3;
        Rww1 = signal_ww1; Rww3 = signal_ww3;

        # save as file names
        fileNameX = 'Rstreamwise';
        fileNameZ = 'Rspanwise';

        # initialize file to write data in streamwise direction
        scfx = open(fileNameX, 'w');
            
        tempRx =  '% \t    1D auto-correlation in streamwise direction\n';
        tempRx += '% \t    current wall-normal plane: '+planeNbr+'\n';
        tempRx += '%     x \t     Ruu1 \t     Rvv1 \t     Rww1\n';
        for i in range(len(x)):
            tempRx += '%e \t %e \t %e \t %e\n'%(x[i], Ruu1[i], Rvv1[i], Rww1[i]);
        
        # write streamwise data to file
        scfx.write(tempRx);
        scfx.close();

        # initialize file to write data in spanwise direction
        scfz = open(fileNameZ, 'w');
        
        tempRz =  '% \t    1D auto-correlation in spanwise direction\n';
        tempRz += '% \t    current wall-normal plane: '+planeNbr+'\n';
        tempRz += '%     z \t     Ruu3 \t     Rvv3 \t     Rww3\n';
        for i in range(len(z)):
            tempRz += '%e \t %e \t %e \t %e\n'%(z[i], Ruu3[i], Rvv3[i], Rww3[i]);

        # write to file
        scfz.write(tempRz);
        scfz.close();


# plot data ?
def visualizeData(someCoords, someData, isOn):
    if isOn:
        plt.loglog(someCoords, someData)
        plt.grid(True)
        plt.show()
    else:
        pass
        



# TODO: compute via conventional autocorrelation R 
# NB: velocity data in format: (Nx, Nz, t)
def bruteForce(Uprime, Vprime, Wprime, x, z):
    # extract dimensions of velocities
    Nx      = len(Uprime);
    Nz      = len(Uprime[0]);
    maxIter = len(Uprime[0][0]);
    
    ## check input is correct
    #assert(Nx, len(Vprime));
    #assert(Nx, len(Wprime));
    #assert(Nz, len(Vprime[0]));
    #assert(Nz, len(Wprime[0]));
    #assert(maxIter, len(Vprime[0][0]));
    #assert(maxIter, len(Wprime[0][0]));

    # initialize R
    Ruu11 = np.zeros(Nx); temp_u1 = np.zeros(Nx);
    Rvv11 = np.zeros(Nx); temp_v1 = np.zeros(Nx);
    Rww11 = np.zeros(Nx); temp_w1 = np.zeros(Nx);
    Ruu33 = np.zeros(Nz); temp_u3 = np.zeros(Nz);
    Rvv33 = np.zeros(Nz); temp_v3 = np.zeros(Nz);
    Rww33 = np.zeros(Nz); temp_w3 = np.zeros(Nz);

    for t in range(maxIter):
        # collapse first from plane to line: x-direction
        temp_u1 = np.zeros(Nx); 
        temp_v1 = np.zeros(Nx); 
        temp_w1 = np.zeros(Nx);
        for i in range(Nz):
            # streamwise direction
            temp_u1 += autoCorr(Uprime[:, i, t], Uprime[:, i, t], Nx);
            temp_v1 += autoCorr(Vprime[:, i, t], Vprime[:, i, t], Nx);
            temp_w1 += autoCorr(Wprime[:, i, t], Wprime[:, i, t], Nx);

        # average by plane
        Ruu11 += np.multiply(temp_u1, 1.0/Nz);
        Rvv11 += np.multiply(temp_v1, 1.0/Nz);
        Rww11 += np.multiply(temp_w1, 1.0/Nz);
        
        # collapse first from plane to line: z-direction
        temp_u3 = np.zeros(Nz);
        temp_v3 = np.zeros(Nz);
        temp_w3 = np.zeros(Nz);
        for j in range(Nx):
            # spanwise direction
            temp_u3 += autoCorr(Uprime[j, :, t], Uprime[j, :, t], Nz);
            temp_v3 += autoCorr(Vprime[j, :, t], Vprime[j, :, t], Nz);
            temp_w3 += autoCorr(Wprime[j, :, t], Wprime[j, :, t], Nz);
        
        # average by plane 
        Ruu33 += np.multiply(temp_u3, 1.0/Nx);
        Rvv33 += np.multiply(temp_v3, 1.0/Nx);
        Rww33 += np.multiply(temp_w3, 1.0/Nx);
       
        # print progress every xth interval
        if (t%10 == 0):
            print 'R: this is the %dth iteration out of %d.'%(t,maxIter); 

   
    # average by time
    Ruu11 = np.multiply(Ruu11, 1.0/maxIter);
    Rvv11 = np.multiply(Rvv11, 1.0/maxIter);
    Rww11 = np.multiply(Rww11, 1.0/maxIter);
    Ruu33 = np.multiply(Ruu33, 1.0/maxIter);
    Rvv33 = np.multiply(Rvv33, 1.0/maxIter);
    Rww33 = np.multiply(Rww33, 1.0/maxIter);

    saveData(x, z, 
             Ruu11, Rvv11, Rww11,
             Ruu33, Rvv33, Rww33, 0,
             planeNbr);




# autocorrelation function R
def autoCorr(f, g, N):
    
    # autocorrelation function
    R = np.zeros(N);
    
    for l in range(N):
        for n in range(N-l):
            R[l] += f[n+l]*g[n]/N;
            
    return R         


