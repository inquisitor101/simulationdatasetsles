# let's import some stuff
import os
import utilities as util

# home directory
home = os.path.expanduser('~');


# this should returns the directory containing the samplerElaborate file
baseDir = os.getcwd(); 

thisDir = baseDir + '/postProcessing/surfaceSampling/';
# all available files
availFiles = os.listdir(thisDir)

# total time steps used in simulation
maxIter = len(availFiles);

# get file names
simFiles = util.getSortedFiles(availFiles, maxIter);


# Nbr of cells
Nx = 128;
Nz = 64;

planeNbr = ['plane0', 'plane1', 'plane2', 'plane3'];

#print len(planeNbr)


for i in range(len(planeNbr)):
    
    # get coordinates of points
    path2File = baseDir+'/postProcessing/surfaceSampling/1010.22/'+planeNbr[i]+'/faceCentres';
   
    # get coordinate values
    xCoord, yCoord, zCoord,\
    rowDim, colDim = util.getCoords(path2File);
    
    # manage data into meaningful lines
    xIdx, zIdx_perX = util.dataCoordinateManager(xCoord, yCoord, zCoord, Nx, Nz);
    # convert array to intergers
    xIdx      = xIdx.astype(int);
    zIdx_perX = zIdx_perX.astype(int);

    # get mean streamwise velocity values of each of the components in the four planes
    path2File = baseDir + '/postProcessing/collapsedFields/';


    # get mean profiles in streamwise direction
    Umean, Vmean, Wmean = util.getMeanVelocity(path2File, i);

    # XXX: debugging mode ON
    #maxIter = 10; ############################ TESTING, DELETE

    # extract total temporal velocity-wise data
    U, V, W = util.getTotalTemporalVelocity(maxIter, simFiles, rowDim,\
                                            colDim, home, i, baseDir);
    
    # # # #
    # Compute and save 1d energy spectral density 
    # # # #
    util.getSpectra(xIdx, zIdx_perX,\
                    U, V, W, maxIter,\
                    Umean, Vmean, Wmean,\
                    xCoord, zCoord, Nx, Nz,\
                    planeNbr[i]);

    print '\n\n'

'''

###########################################################
# get coordinates of points
###########################################################
path2File = baseDir + '/postProcessing/surfaceSampling/1010.22/plane0/faceCentres';
#path2File = baseDir + '/simulationChamber/postProcessing/surfaceSampling/1000.01/plane0/faceCentres';

# get coordinate values
xCoord, yCoord, zCoord, rowDim, colDim = util.getCoords(path2File);

# manage data into meaningful lines
# returned data are indices.
# xIdx are the 258 indices of each X-coordinate (0<xCoord[xIdx]<9)
# zIdx_perX are the 129 Z-coordinate indices of each one of the 258 X-coordinate indices, hence zIdx_perX has 258-by-129, with each one of the 258 row entries being equal to xIdx (0<zCoord[zIdx_perX]<4)
xIdx, zIdx_perX = util.dataCoordinateManager(xCoord, yCoord, zCoord, Nx, Nz);
# convert array to intergers
xIdx      = xIdx.astype(int);
zIdx_perX = zIdx_perX.astype(int);
###########################################################
###########################################################


###########################################################
# get mean streamwise velocity values of each of the components in the four planes
###########################################################
path2File = baseDir + '/postProcessing/collapsedFields/';

# get mean profiles in streamwise direction
Umean0, Vmean0, Wmean0, Umean1, Vmean1, Wmean1, Umean2, Vmean2, Wmean2, Umean3, Vmean3, Wmean3 = util.getMeanVelocity(path2File);
###########################################################
###########################################################

# XXX: debugging mode ON
maxIter = 10; ############################ TESTING, DELETE

###########################################################
# extract total temporal velocity-wise data
###########################################################
planeNbr = 0; # yPlus ~ 5
U, V, W = util.getTotalTemporalVelocity(maxIter, simFiles, rowDim, colDim, home, planeNbr, baseDir);
###########################################################
###########################################################



# # # #
# Compute and save 1d energy spectral density 
# # # #
util.getSpectra(xIdx, zIdx_perX, U, V, W, maxIter, Umean0, Vmean0, Wmean0, xCoord, zCoord, Nx, Nz);

'''


