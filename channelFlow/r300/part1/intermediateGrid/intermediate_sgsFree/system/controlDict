/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  3.0.1                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     	pimpleFoam;

startFrom       	latestTime;

startTime       	0;

stopAt          	endTime;

endTime         	850;

deltaT          	0.01;

writeControl    	timeStep;

writeInterval   	100;

purgeWrite      	2;

writeFormat     	ascii;

writePrecision  	6;

writeCompression 	off;
 
timeFormat      	general;

timePrecision   	7;

runTimeModifiable 	true;

// adjustTimeStep		yes;

// maxCo			0.5;

functions
{
    profiles
    {
	type			sets;
	enabled			true;
	verbose			true;
	outputControlMode	outputTime;
	outputInterval		50;
	interpolationScheme	cellPoint;
	enabled			true;
	setFormat		raw;
	fields			
	(
		U
	);
	sets
	(

		// streamwise direction
		lineX_0  // yPlus = 5 
		{
			type	    	midPoint; //uniform;
			axis		x;
			start		(0 0.016667 2.01);
			end		(9 0.016667 2.01);
			nPoints		64;
		}

		lineX_1  // yPlus = 15
		{
			type	    	midPoint; //uniform;
			axis		x;
			start		(0 0.05 2.01);
			end		(9 0.05 2.01);
			nPoints		64;
		}

		lineX_2  // y = \delta/2
		{
			type	    	midPoint; //uniform;
			axis		x;
			start		(0 0.5 2.01);
			end		(9 0.5 2.01);
			nPoints		64;
		}

		lineX_3  // y = \delta
		{
			type	    	midPoint; //uniform;
			axis		x;
			start		(0 1 2.01);
			end		(9 1 2.01);
			nPoints		64;
		}


		// spanwise direction
		lineZ_0  // yPlus = 5 
		{
			type	    	midPoint; //uniform;
			axis		z;
			start		(4.5 0.016667 0);
			end		(4.5 0.016667 4);
			nPoints		64;
		}

		lineZ_1  // yPlus = 15
		{
			type	    	midPoint; //uniform;
			axis		z;
			start		(4.5 0.05 0);
			end		(4.5 0.05 4);
			nPoints		64;
		}

		lineZ_2  // y = \delta/2
		{
			type	    	midPoint; //uniform;
			axis		z;
			start		(4.5 0.5 0);
			end		(4.5 0.5 4);
			nPoints		64;
		}

		lineZ_3  // y = \delta
		{
			type	    	midPoint; //uniform;
			axis		z;
			start		(4.5 1 0);
			end		(4.5 1 4);
			nPoints		64;
		}
	);
    }


    uTau    
    {
        type 			patchExpression;
        patches 		( bottomWall topWall);
        outputControlMode 	timeStep;
        outputInterval 		1;
        expression 		"sqrt(nu*mag(snGrad(U)))";
        verbose 		true;
        accumulations 		( average );
    }

    yPlus   
    {
        type 			patchExpression;
        patches 		( bottomWall topWall);
        outputControlMode 	timeStep;
        outputInterval 		1;
        expression 		"dist()/nu*sqrt(nu*mag(snGrad(U)))";
        verbose 		true;
        accumulations 		( average );
    }

    ReTau   
    {
        type 			patchExpression;
        patches 		( bottomWall topWall);
        outputControlMode 	timeStep;
        outputInterval 		1;
        expression 		"sqrt(nu*mag(snGrad(U)))/nu";
        verbose 		true;
        accumulations 		( average );
    }

    fieldAverage
    {
        type            	fieldAverage;
        functionObjectLibs 	( "libfieldFunctionObjects.so" );
        enabled         	true;
        outputControl   	outputTime;
        resetOnRestart 		false;

        fields
        (
            U
            {
                mean        on;
                prime2Mean  on;
                base        time;
            }
            p
            {
                mean        on;
                prime2Mean  on;
                base        time;
            }
	    nut
	    {
		mean	    on;
		prime2Mean  off;
		base	    time;
	    }
        );
    }


    sampledSurface
    {
        type 			surfaces;
        outputControl 		timeStep;
        enabled 		false;
        outputInterval 		1;

        surfaceFormat 		foamFile;
        interpolationScheme 	none;
        interpolate 		false;
        triangulate 		false;

        fields
        (
            U
        );

        surfaces 
        (
            inletSurface
            {
                type patch;
                patches (inlet);
            }
        );
    }
}

libs (
    "libOpenFOAM.so"
    "libsimpleFunctionObjects.so"
    "libsimpleSwakFunctionObjects.so"
    "libswakFunctionObjects.so"
    );

// ************************************************************************* //
