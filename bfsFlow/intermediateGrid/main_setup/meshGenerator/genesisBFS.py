# # # #
#
# This script is developed for generating a BFS 
# (backward-facing step) mesh in OpenFOAM for 
# blockMeshDict dictionaries. 
#
# Developer: 
#            inquisitor101                                        #
#                                                                 #
#                                                                 #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

# let's import some stuff
import utilities as util


# OpenFOAM version number
OFversion = '3.0.1'; 

# # #
# get blue prints for cells and parameters
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
inputFile = 'bluePrint.csv';


# get all required design parameters from inputFile
Rx0, Rx1, Rx2, Rx3, Rx4, Rx5, Rx6, Ry0, Ry1, Ry2, dyPlus1, ReTau, Lx, Ly, Lz, Lx1, Ly1, cellsX, cellsY, cellsZ, increasedRatio, delta  =   util.readInput(inputFile); 

# overall mesh parameters per side
Nblocks = 54;         # number of blocks
Nnodes  = 71;         # number of nodes per left or right side

# scale converter (to meters)
scale = 1;

# use fast merge? 1(yes), else 0 (no)
fastMerge = 1;

# prepare case by setting up all required geometry 
x_A, y_A, x_B, y_B, Lx0, Ly0, dy1, isExpandableR, isExpandableC  =   util.prepareCase(Rx0, Rx1, Rx2, Rx3, Rx4, Rx5, Rx6, Ry0, Ry1, Ry2, dyPlus1, ReTau, Lx, Ly, Lz, Lx1, Ly1, cellsX, cellsY, cellsZ, Nblocks, Nnodes, increasedRatio, cellsY[3], delta);

# start assigning indices to blocks
inletFace, outletFace, stepFace, topFace, bottomFace, leftFace, rightFace, x, y, z    =   util.assignBlockIdx(x_A, y_A, x_B, y_B, Lx, Ly, Lz, Nnodes);


# return number of cells and grading coefficients
block, NcellsX, NcellsY, NcellsZ, Gx, Gy, Gz =   util.constructHex(delta, dy1, ReTau, Lx, Ly, Lz, Lx1, Ly1, cellsX, cellsY, cellsZ, leftFace, rightFace, Nblocks, isExpandableR, isExpandableC, Rx0, Rx1, Rx2, Rx3, Rx4, Rx5, Rx6, Ry0, Ry1, Ry2, Ly0, Lx0, increasedRatio);

# create blockMeshDict file 
util.createBlockMeshDict(block, NcellsX, NcellsY, NcellsZ, Gx, Gy, Gz, x, y, z, Nblocks, inletFace, outletFace, stepFace, bottomFace, topFace, leftFace, rightFace, scale, fastMerge, OFversion, Nnodes);



