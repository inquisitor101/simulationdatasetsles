# # #
# this is a python script used to calculate nonlinear
# systems of equations of 2 equations, 2 unknowns   
# using SciPy's fsolve functionality..               
#
# function inputs: 
#                  Ltot: total block length
#                  x0  : width of first cell            #
#                  xn  : width of last  cell            #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # #
import math
from numpy import *
from scipy.optimize import fsolve

def getNcells(Ltot, x0, xn):
	# set initial guess
	zGuess = [1.75, 20];
	
	def setFunction(zGuess):
		# set initial guess
		q = zGuess[0];  # cell-to-cell expansion ratio
		n = zGuess[1];  # total number of cells 


		# function solution
		F = empty((2));
	
		# re-arrange as: F = 0	
		F[0] = pow(q, n) - (Ltot/x0)*q + (Ltot/x0) - 1.0;
		F[1] = pow(q, n-1) - (xn/x0);
	
		return F

	# solve for solution
	z = fsolve(setFunction, zGuess);
	
	return math.ceil(z[1])
