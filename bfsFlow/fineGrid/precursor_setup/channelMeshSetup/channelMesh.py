from __future__ import division
from subprocess import call
from PyFoam.Basics.TemplateFile import TemplateFile
from PyFoam.RunDictionary.SolutionDirectory import SolutionDirectory
from os import path,rename,getcwd
import numpy as np
import sys
import math
import nonLinearSolver as mySolver



case = SolutionDirectory(".", archive = None, paraviewLink = False )


# correlation: 1 = Dean's, 2 = Clark's
thisCorrelation = 1;


if thisCorrelation == 1:
    thisRelation = 'Dean\'s';
elif thisCorrelation == 2:
    thisRelation = 'Clark\'s';
else:
    sys.exit("error input while choosing a correlation for ReTau!");

# 3 layer-mesh
is3Layers = 'true';

# define best-fit function from Timofey's inflow generation paper
def f_inflow(ReTheta, isRelation):
    # this outputs f1
    if isRelation == 1:
        alpha =  0.3427; 
        beta  = -0.1287;
        gamma =  1.000;
    # this outputs f2    
    elif isRelation == 2:
        alpha =  2.603e-4;
        beta  =  0.9834;
        gamma =  11.28;
    # in case of wrong input !
    else:
        sys.exit("error input in f_inflow function !"); 
    
    f = gamma + alpha*(ReTheta**beta);
    return f

# define correlation between ReTau and Re_b
def estimateRe(Re_b, isRelation):
    # this uses Dean's correlation
    if isRelation == 1:
        ReTau = 0.1751*(Re_b**(0.875));
    # this uses Clark's correlation
    elif isRelation == 2:
        ReTau = 0.1079*(Re_b**(0.911));
    # in case of wrong input !
    else:
        sys.exit("error input in correlateRe function !");

    return ReTau
        
# specify step-height Re number
Re_h    = 5100;

# specify momentum thickness Re number
ReTheta = 670; 

# specify kinematic viscosity used
nu      = 2.0e-4;

# specify BFS step-height
h       = 0.5;

# compute max mean-inlet of BFS velocity U0
U0      = Re_h*nu/h;

# assuming U0 = Uc (free-stream velocity ~ at channel half-height)
theta   = ReTheta*nu/U0;

# compute channel half-height, delta
delta   = theta*f_inflow(ReTheta, 2);

# compute Ub, 
Ub      = U0/f_inflow(ReTheta, 1);

# calculate Re_b (Re bulk)
Re_b    = delta*Ub/nu;


# estimate using a correlation ReTau from Re_b
ReTau = estimateRe(Re_b, thisCorrelation);

# specify first cell width (= 2*cellCenter) distance w.r.t. wall
dyPlus1 = 2.0; # this means, in FVM, cell-center is at dy1+ = 1

# channel dimensions (function of channel-half width)
Lx = 9*delta;
Ly = 2*delta;   
Lz = 4*h;

# compute first cell's physical location
dy1 = dyPlus1*delta/ReTau;

# wall-normal height 
y0 = 3*dy1;      # bottom: sub-viscuous layer taken at y+=6, not y+=5 (end w.r.t. bottom wall)
y1 = 0.2*delta;  # bottom: overlap layer (start w.r.t. bottom wall)
y2 = 1.8*delta;  # top   : overlap layer (start w.r.t. top wall)
y3 = Ly - y0;    # top   : sub-viscuous layer taken at y+=6, not y+=5 (end w.r.t. top wall) 

# which method to use to construct mesh in X and Z directions? 
#isManualNcells = 1; # use exact number of cells 
isManualNcells = 0; # compute cells based on dimensionless parameters
if isManualNcells:
    Nx = 120;
    Nz = 30;

    dxPlus = (Lx/Nx)*ReTau/delta;
    dzPlus = (Lz/Nz)*ReTau/delta;
else:
    # specify dxPlus
    dxPlus  = 10;
    
    # specify dzPlus
    dzPlus  = 5;
    
    # number of (uniform) cells in streamwise direction
    Nx  = int(math.ceil(Lx/(dxPlus*delta/ReTau)));
    
    # number of (uniform) cells in spanwise direction
    Nz  = int(math.ceil(Lz/(dzPlus*delta/ReTau)));

# number of (uniform) cells in wall-normal sub-viscuous layer
Ny0 = 3;

# number of (uniform) cells in wall-normal outer layer
Ny2 = 20;

# solve for number of cells and (geometric) expansion ratio
cell0 = dy1;            # width of first cell         (sub-viscuous region)
celln = 0.8*delta/Ny2;  # width of final cell         (outer region)
dist  = 0.2*delta - y0; # length of considered region (overlap region)

# get number of cells in overlap layer
Ny1 = int(mySolver.getNcells(dist, cell0, celln));

# get simpleGrading ratio
Gb = celln/cell0; # near bottom wall
Gt = 1.0/Gb;      # near top wall


# print detailed information
Ny          = 2.0*(Ny0+Ny1+Ny2);
totCells    = Nx*Ny*Nz;
perProc     = 20000;
totCores    = totCells/perProc; 

print ('\nReTau correlation used:        \t %s'
       '\nfriction Re:           ReTau = \t %10.6f'
       '\nbulk Re:                Re_b = \t %10.6f'
       '\nstep-height Re:         Re_h = \t %10.6f'
       '\nmomentum Re:         ReTheta = \t %10.6f'
       '\nviscosity:                nu = \t %e'
       '\nstep-heigh:                h = \t %10.6f' 
       '\nchannel half-height:   delta = \t %10.6f'
       '\nfirst y-cell center:    dy1+ = \t %10.6f'
       '\ncell x-width:            dx+ = \t %10.6f'
       '\ncell z-width:            dz+ = \t %10.6f'
       '\nbulk velocity:            Ub = \t %10.6f'
       '\nfree-stream velocity:     U0 = \t %10.6f'
       '\nmomentum thickness:    theta = \t %10.6f'
       '\ndimensions:     (Lx, Ly, Lz) = \t (%10.6f, %10.6f, %10.6f)'
       '\ncell number:    (Nx, Ny, Nz) = \t (%10d, %10d, %10d)'
       '\ntotal number of cells:     N = \t %d'
       '\nwall-normal mesh:   3-layer? = \t %s'
       '\n# cells in subviscous layer:   \t %d'
       '\n# cells in overlap layer:      \t %d'
       '\n# cells in outer layer:        \t %d' 
       '\n---------------------------------'
       '\nEstimated number of cores required -- assuming an ideal load of 20k/processor: \nNcores = \t %8.3f'
       %(thisRelation, ReTau, Re_b, Re_h, ReTheta, nu, 
         h, delta, dyPlus1/2, dxPlus, dzPlus, Ub, U0, theta, 
         Lx, Ly, Lz, Nx, Ny, Nz, totCells, is3Layers, 
         Ny0, Ny1, Ny2, totCores));



# let's start building, eh?
print "Generating blockMeshDict\n";

paramString =       \
"{'Lx':"+str(Lx)+   \
",'Lz':"+str(Lz)+   \
",'Ly':"+str(Ly)+   \
",'Nx':"+str(Nx)+   \
",'Ny0':"+str(Ny0)+ \
",'Ny1':"+str(Ny1)+ \
",'Ny2':"+str(Ny2)+ \
",'Nz':" +str(Nz)+  \
",'Gb':"+str(Gb)+   \
",'Gt':"+str(Gt)+   \
",'y0':"+str(y0)+   \
",'y1':"+str(y1)+   \
",'y2':" +str(y2)+  \
",'y3':"+str(y3)+   \
",'delta':"+str(delta)+ \
"}"

# current directory (assumed along with system/ constant/ 0.orig/ ...etc)
baseDir  = getcwd();
meshFile = path.join(baseDir, "channelMeshSetup","blockMeshDict"); 

# call(["pyFoamFromTemplate.py", "constant/polyMesh/blockMeshDict", paramString])
call(["pyFoamFromTemplate.py", meshFile, paramString])

# change dictionary to system/.
targetFile = path.join(baseDir, "system/blockMeshDict");

# move file
rename(meshFile, targetFile);
