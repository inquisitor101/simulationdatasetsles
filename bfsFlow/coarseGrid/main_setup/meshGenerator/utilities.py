import math 
import os
import numpy as np
import csv

from nonLinearSolver import *


# extract information according to bluePrint.csv file
def readInput(inputFile):
    r_idx  = 1;
    cellsX = [];
    cellsY = [];
    # current directory
    baseDir  = os.getcwd();
    # include directory and file
    thisFile = baseDir + '/meshGenerator/' + inputFile; 

    with open(thisFile, 'rb') as bluePrintFile:
        reader = csv.reader(bluePrintFile, delimiter=',');
        for row in reader:
            if r_idx == 9:
                for i in range(len(row)):
    	   	    tempY = row[i].strip().split();
    	   	    cellsY.append(tempY);
    	    if r_idx == 13:
    	    	for j in range(len(row)):
    	   	    tempX = row[j].strip().split();
    	   	    cellsX.append(tempX);
    	    if r_idx == 16:
    	        cellsZ = float(row[0]);
    	   	
    	    if r_idx == 20:
                # total BFS streamwise distance
    		Lx = float(row[0]);
    	   	# total BFS wall-normal distance
    	   	Ly = float(row[1]);
    	   	# total BFS spanwise distance
    	   	Lz = float(row[2]);
    	   	
    	    if r_idx == 24:
    	   	# major horizontal length section
    	   	Lx1 = float(row[0]);
    	   	# major vertical length section
    	   	Ly1 = float(row[1]);
    	   
    	    if r_idx == 28:
    	        # ratios w.r.t. Lx0
    	   	Rx0 = float(row[0]); # 1st increment X
    	   	Rx1 = float(row[1]); # 2nd increment X
    	   	Rx2 = float(row[2]); # 3rd increment X
    	   	# ratios w.r.t. Lx1
    	   	Rx3 = float(row[3]); # 4th increment X
    	   	Rx4 = float(row[4]); # 5th increment X
    	   	Rx5 = float(row[5]); # 6th increment X
    	   	Rx6 = float(row[6]); # 7th increment X
    	   	# ratios w.r.t. Ly0
    	   	Ry0 = float(row[7]); # 1st increment Y
    	   	Ry1 = float(row[8]); # 2nd increment Y
    	   	Ry2 = float(row[9]); # 3rd increment Y
            if r_idx == 31:
                # target dyPlus1: 2x dyPlus1 to account for entire FV cell
    	   	dyPlus1 = 2.0*float(row[0]);
            if r_idx == 34:
    	   	# target friction Re
    	   	ReTau = float(row[0]);		
       	   	# moveto next line
            if r_idx == 37:
                # increase top-most cell width ratio
                increasedRatio = float(row[0]);
            if r_idx == 40:
                # channel half-height
                delta = float(row[0]);
            r_idx += 1;
     
    # # # # # # code check  # # # # # # # # # # # # # # # # # # #
    np.testing.assert_almost_equal(Rx0 + Rx1 + Rx2, 1.0);       #
    np.testing.assert_almost_equal(Rx3 + Rx4 + Rx5 + Rx6, 1.0); #
    np.testing.assert_almost_equal(Ry0 + Ry1 + Ry2, 1.0);       #
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
    
    return Rx0, Rx1, Rx2, Rx3, Rx4, Rx5, Rx6, Ry0, Ry1, Ry2, dyPlus1, ReTau, Lx, Ly, Lz, Lx1, Ly1, cellsX, cellsY, cellsZ, increasedRatio, delta


# set-up additional geometry requirements before meshing
def prepareCase(Rx0, Rx1, Rx2, 
                Rx3, Rx4, Rx5, Rx6, 
                Ry0, Ry1, Ry2,
                dyPlus1, ReTau,
                Lx, Ly, Lz, Lx1, Ly1,
                cellsX, cellsY, cellsZ, 
                Nblocks, Nnodes, increasedRatio, nOuterCells, delta):
    
    
    # check who needs expansions ! ! 
    isX = 'x';
    isExpandableR = np.zeros(9);
    isExpandableC = np.zeros(7);
    
    for i in range(len(cellsY)):
    	if isX in cellsY[i]:
    	    isExpandableR[i] = 1;
    	else:
    	    cellsY[i] = float(cellsY[i][0]);
    
    for j in range(len(cellsX)):
    	if isX in cellsX[j]:                                    
    	    isExpandableC[j] = 1;                           
    	else:                                                   
    	    cellsX[j] = float(cellsX[j][0]);                
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
    
    Lx0 = Lx - Lx1;
    Ly0 = Ly - Ly1;
    
    # initiate channel-like block (i.e. top block)
    dy1        = dyPlus1*delta/ReTau;      # convert back to dimensional parameters
    y1_inner   = 3.0*dy1;                  # inner region assumed at y+=6  wall-normal distance 
    y1_overlap = 0.2*delta - y1_inner;     # overlap region wall-normal distance  
    y1_outer   = 0.8*delta;                # outer region assumed at y+=6  wall-normal distance
    
    nOuterCells = float(nOuterCells[0]);
    
    y1_outerTop   = y1_outer/nOuterCells; 
    y1_innerTop   = y1_outerTop*increasedRatio;
    y1_overlapTop = (Ly1 - delta) - (y1_outerTop + y1_innerTop);
    
    # assert everything's fine !
    np.testing.assert_almost_equal(y1_inner + y1_overlap + y1_outer, delta);
    np.testing.assert_almost_equal(y1_outerTop + y1_innerTop + y1_overlapTop + y1_inner + y1_overlap + y1_outer, Ly1);
    
    dx_A = [0.0, Lx0*Rx0, Lx0*Rx1, Lx0*Rx2, Lx1*Rx3, Lx1*Rx4, Lx1*Rx5, Lx1*Rx6]; 
    dy_A = [0.0, y1_innerTop, y1_overlapTop, y1_outerTop, y1_outer, y1_overlap, y1_inner];

    
    dx_B = [0.0, Lx1*Rx3, Lx1*Rx4, Lx1*Rx5, Lx1*Rx6];
    dy_B = [0.0, Ly0*Ry0, Ly0*Ry1, Ly0*Ry2];
    

    # NB: 2 is because one for the left and right faces
    x = np.zeros(2*Nnodes); # x-coordinates
    y = np.zeros(2*Nnodes); # y-coordinates
    z = np.zeros(2*Nnodes); # z-coordinates
    
    # section A starting point
    x0 = 0.0 - Lx0;
    y0 = Ly1;
    
    # coordinates of blocks in section A
    temp = x0;
    x_A  = np.zeros(8);
    for i in range(8):
    	temp  += dx_A[i];
    	x_A[i] = temp;
    
    temp = y0;
    y_A  = np.zeros(7);
    for j in range(7):
    	temp  -= dy_A[j];
    	y_A[j] = temp; 
    
    # section B starting point
    x0 = 0.0;
    y0 = 0.0;
      
    # coordinates of blocks in section B
    temp = x0;
    x_B  = np.zeros(5);
    for i in range(5):
    	temp  += dx_B[i];
    	x_B[i] = temp;
    
    temp = y0;
    y_B  = np.zeros(3);
    for j in range(3):
    	temp  -= dy_B[j+1];
    	y_B[j] = temp;

    return x_A, y_A, x_B, y_B, Lx0, Ly0, dy1, isExpandableR, isExpandableC 



# assign block indices
def assignBlockIdx(x_A, y_A, x_B, y_B, Lx, Ly, Lz, Nnodes):
    
    # loop over section A -- i.e. blocks b01,...,b43 consecutively !
    idx = 0;
    
    # NB: 2 is because one for the left and right faces
    x = np.zeros(2*Nnodes); # x-coordinates
    y = np.zeros(2*Nnodes); # y-coordinates
    z = np.zeros(2*Nnodes); # z-coordinates
 
    # indices for surfaces
    # # # # # # # # # # # # # # # # # # # # #
    idx_Tl     = 0;
    top_l      = np.zeros(8);
    idx_Tr     = 0;
    top_r      = np.zeros(8);
    
    idx_Il     = 0;
    inlet_l    = np.zeros(7);
    idx_Ir     = 0;
    inlet_r    = np.zeros(7);
    
    idx_Bal    = 0;
    bottomA_l  = np.zeros(4);
    idx_Bar    = 0;
    bottomA_r  = np.zeros(4);
    
    idx_Oal    = 0;
    outletA_l  = np.zeros(7);
    idx_Oar    = 0;
    outletA_r  = np.zeros(7);
    
    idx_Sl     = 1;
    step_l     = np.zeros(4);
    idx_Sr     = 1;
    step_r     = np.zeros(4);
    
    idx_Bbl    = 0;
    bottomB_l  = np.zeros(5);
    idx_Bbr    = 0;
    bottomB_r  = np.zeros(5);
    
    idx_Obl    = 1;
    outletB_l  = np.zeros(4);
    idx_Obr    = 1;
    outletB_r  = np.zeros(4);
    # # # # # # # # # # # # # # # # # # # # #
    for k in range(2):
    	for j in range(7):
    	    for i in range(8):
    	        idx = i + j*8 + k*56;
    		x[idx] = x_A[i];
    		y[idx] = y_A[j];
    		z[idx] = k*Lz;
    			
    		if j == 0:
    		    if k == 0:
    			top_l[idx_Tl] = idx;
    			idx_Tl += 1;
    		    else:
    			top_r[idx_Tr] = idx;
    			idx_Tr += 1;
    		if i == 0:
    		    if k == 0:
    			inlet_l[idx_Il] = idx;
    			idx_Il += 1;
    		    else:
    			inlet_r[idx_Ir] = idx;
    			idx_Ir += 1;
    		if i < 4 and j == 6:
    		    if k == 0:
    			bottomA_l[idx_Bal] = idx;
    			idx_Bal += 1;
    		    else:
    			bottomA_r[idx_Bar] = idx;
    			idx_Bar += 1;
    		if i == 7:
    		    if k == 0:
    			outletA_l[idx_Oal] = idx;
    			idx_Oal += 1;
    		    else:
    			outletA_r[idx_Oar] = idx;
    			idx_Oar += 1;
    
    # # # face connection fix # #
    step_l[0]    = bottomA_l[3];#
    step_r[0]    = bottomA_r[3];#
    outletB_l[0] = outletA_l[6];# 
    outletB_r[0] = outletA_r[6];#
    # # # # # # # # # # # # # # #
    
    # loop over section B -- i.e. blocks b44,...,b55 consecutively !
    for k in range(2):
    	for j in range(3):
    	    for i in range(5):
    		idx += 1;
    		x[idx] = x_B[i];
    		y[idx] = y_B[j];
    		z[idx] = k*Lz;
    			
    		if i == 0:
    		    if k == 0:
    		        step_l[idx_Sl] = idx;
    			idx_Sl += 1;
    		    else:
    			step_r[idx_Sr] = idx;
    			idx_Sr += 1;
    		if j == 2:
    		    if k == 0:
    		        bottomB_l[idx_Bbl] = idx;
    			idx_Bbl += 1;
    		    else:
    			bottomB_r[idx_Bbr] = idx;
    			idx_Bbr += 1;
    
    		if i == 4:
    	            if k == 0:
    		        outletB_l[idx_Obl] = idx;
    			idx_Obl += 1;
    		    else:
    			outletB_r[idx_Obr] = idx;
    			idx_Obr += 1;

    # get BC indices of all surfaces 
    inletFace, outletFace, stepFace, topFace, bottomFace, leftFace, rightFace   =   manageIdxBC(inlet_r, inlet_l, outletA_r, outletA_l, outletB_r, outletB_l, step_r, step_l, top_r, top_l, bottomA_r, bottomA_l, bottomB_r, bottomB_l);

    return inletFace, outletFace, stepFace, topFace, bottomFace, leftFace, rightFace, x, y, z


# manange boundary face indices
def manageIdxBC(inlet_r, inlet_l, 
                outletA_r, outletA_l, 
                outletB_r, outletB_l, 
                step_r, step_l, 
                top_r, top_l, 
                bottomA_r, bottomA_l, 
                bottomB_r, bottomB_l):

    # get all required indices for the boundary faces
    inletFace  = getInletIdx(inlet_r, inlet_l);
    outletFace = getOutletIdx(outletA_r, outletA_l, outletB_r, outletB_l);
    stepFace   = getStepIdx(step_r, step_l);
    topFace    = getTopIdx(top_r, top_l);
    bottomFace = getBottomIdx(bottomA_r, bottomA_l, bottomB_r, bottomB_l);
    leftFace   = getLeftIdx();
    rightFace  = getRightIdx();

    return inletFace, outletFace, stepFace, topFace, bottomFace, leftFace, rightFace



# compute and return the inlet's indices
def getInletIdx(inlet_r, inlet_l):
    inletFace = np.zeros((6,4));
    k = 0;
    for i in range(6):
    	inletFace[i, 0] = inlet_r[1 + k];
    	inletFace[i, 1] = inlet_r[k];
    	inletFace[i, 2] = inlet_l[k];
    	inletFace[i, 3] = inlet_l[1 + k];
    	k += 1;

    return inletFace


# compute and return the outlet's indices
def getOutletIdx(outletA_r, outletA_l, 
                 outletB_r, outletB_l):
    outletFace = np.zeros((9, 4));
    k = 0;
    for i in range(6):
    	outletFace[i, 0] = outletA_l[1 + k]; 
    	outletFace[i, 1] = outletA_l[k];
    	outletFace[i, 2] = outletA_r[k];
    	outletFace[i, 3] = outletA_r[1 + k];
    	k += 1;
    k = 0;
    for i in range(3):
    	outletFace[6 + i, 0] = outletB_l[1 + k]; 
    	outletFace[6 + i, 1] = outletB_l[k];
    	outletFace[6 + i, 2] = outletB_r[k];
    	outletFace[6 + i, 3] = outletB_r[1 + k];
    	k += 1;

    return outletFace


# compute and return the step-height's indices
def getStepIdx(step_r, step_l):
    stepFace = np.zeros((3, 4));
    k = 0;
    for i in range(3):
    	stepFace[i, 0] = step_r[1 + k]; 
    	stepFace[i, 1] = step_r[k];
    	stepFace[i, 2] = step_l[k];
    	stepFace[i, 3] = step_l[1 + k];
    	k += 1;

    return stepFace


# compute and return the top indices
def getTopIdx(top_r, top_l):
    topFace = np.zeros((7, 4));
    k = 0;
    for i in range(7):
    	topFace[i, 0] = top_l[1 + k]; 
    	topFace[i, 1] = top_l[k];
    	topFace[i, 2] = top_r[k];
    	topFace[i, 3] = top_r[1 + k];
    	k += 1;

    return topFace


# compute and return the bottom indices
def getBottomIdx(bottomA_r, bottomA_l, 
                 bottomB_r, bottomB_l):
    bottomFace = np.zeros((7, 4));
    k = 0;
    for i in range(3):
    	bottomFace[i, 0] = bottomA_l[k];
    	bottomFace[i, 1] = bottomA_l[1 + k];
    	bottomFace[i, 2] = bottomA_r[1 + k];
    	bottomFace[i, 3] = bottomA_r[k];
    	k += 1;
    k = 0;
    for i in range(4):
    	bottomFace[3 + i, 0] = bottomB_l[k];
    	bottomFace[3 + i, 1] = bottomB_l[1 + k];
    	bottomFace[3 + i, 2] = bottomB_r[1 + k];
    	bottomFace[3 + i, 3] = bottomB_r[k];
    	k += 1;

    return bottomFace


# compute and return the left's indices
def getLeftIdx():
    # blocks b01,...,b42
    leftFace = np.zeros((54, 4));
    k = 0;
    for i in range(42):
    	if i%7 == 0 and i>0:
    	    k +=1;
    	leftFace[i, 0] = i + k;
    	leftFace[i, 1] = i + 1 + k; 
    	leftFace[i, 2] = i + 9 + k; 
    	leftFace[i, 3] = i + 8 + k;
    	
    
    # blocks b43,...,b46 (index discontinuity between sections A & B)
    for i in range(4):
    	leftFace[42 + i, 0] = 51 + i;  
    	leftFace[42 + i, 1] = 51 + i + 1; 
    	leftFace[42 + i, 2] = 107 + i + 6; 
    	leftFace[42 + i, 3] = 107 + i + 5;
    
    # blocks b47,...,b54 
    k = 0;
    for i in range(8):
    	if i%4 == 0 and i>3:
    	    k += 1;
    	leftFace[46 + i, 0] = 112 + i + k; 
    	leftFace[46 + i, 1] = 112 + i + 1 + k;
    	leftFace[46 + i, 2] = 112 + i + 6 + k;
    	leftFace[46 + i, 3] = 112 + i + 5 + k;

    return leftFace


# compute and return the right's indices
def getRightIdx():
    rightFace = np.zeros((54, 4)); 
    k = 0;
    for i in range(42):
    	if i%7 == 0 and i>0:
    	    k +=1;
    	rightFace[i, 0] = 56 + 8 + i + k; 
    	rightFace[i, 1] = 56 + 9 + i + k;
    	rightFace[i, 2] = 56 + 1 + i + k;
    	rightFace[i, 3] = 56 + i + k;
    
    for i in range(4):
    	rightFace[42 + i, 0] = 122 + 5 + i;
    	rightFace[42 + i, 1] = 122 + 6 + i;
    	rightFace[42 + i, 2] = 107 + 1 + i;
    	rightFace[42 + i, 3] = 107 + i;
    
    k = 0;
    for i in range(8):
    	if i%4 == 0 and i>3:
    	    k += 1;
    	rightFace[46 + i, 0] = 127 + 5 + i + k; 
    	rightFace[46 + i, 1] = 127 + 6 + i + k;
    	rightFace[46 + i, 2] = 127 + 1 + i + k;
    	rightFace[46 + i, 3] = 127 + i + k;

    return rightFace



# construct hrx blocks
def constructHex(delta, dy1, ReTau, Lx, 
                 Ly, Lz, Lx1, Ly1, cellsX, 
                 cellsY, cellsZ, leftFace, 
                 rightFace, Nblocks, 
                 isExpandableR, isExpandableC, 
                 Rx0, Rx1, Rx2, Rx3, Rx4, 
                 Rx5, Rx6, Ry0, Ry1, 
                 Ry2, Ly0, Lx0, increasedRatio):
    
    # constuct the block numbering -- i.e. hex 
    block = np.zeros((Nblocks, 8));
    for i in range(Nblocks):
    	for j in range(4):
    	    block[i, j] = leftFace[i, j];	 
    	for k in range(4):
    	    block[i, k+4] = rightFace[i, 3-k];
    
    #################################################################################################
    # decompose blocks into columns (C0,...,C6) and rows (R0,...,R8)
    # each column or row, C or R, is assigned it's subset block (block = hex)
    C0 = np.array([0,  7, 14, 21, 28, 35]);
    C1 = np.array([1,  8, 15, 22, 29, 36]);
    C2 = np.array([2,  9, 16, 23, 30, 37]);
    C3 = np.array([3, 10, 17, 24, 31, 38, 42, 46, 50]);  
    C4 = np.array([4, 11, 18, 25, 32, 39, 43, 47, 51]);
    C5 = np.array([5, 12, 19, 26, 33, 40, 44, 48, 52]);
    C6 = np.array([6, 13, 20, 27, 34, 41, 45, 49, 53]);
    
    R0 = np.array([ 0,  1,  2,  3,  4,  5,  6]);
    R1 = np.array([ 7,  8,  9, 10, 11, 12, 13]);
    R2 = np.array([14, 15, 16, 17, 18, 19, 20]);
    R3 = np.array([21, 22, 23, 24, 25, 26, 27]);
    R4 = np.array([28, 29, 30, 31, 32, 33, 34]);
    R5 = np.array([35, 36, 37, 38, 39, 40, 41]);
    R6 = np.array([42, 43, 44, 45]);
    R7 = np.array([46, 47, 48, 49]);
    R8 = np.array([50, 51, 52, 53]);
    
    ###############################################################################################
    ############################################################################### NEEDS FIXING !! 
    NcellsX = np.zeros(Nblocks);
    NcellsY = np.zeros(Nblocks);
    NcellsZ = np.zeros(Nblocks);
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # y section (start)
    # inner layer - section A
    for i in range(len(R0)):
    	NcellsY[R0[i]] = cellsY[0];
    assert isExpandableR[0] == 0;
    
    for i in range(len(R5)):
    	NcellsY[R5[i]] = cellsY[5];
    assert isExpandableR[5] == 0;
    
    # outer layer - section A
    for i in range(len(R2)):
    	NcellsY[R2[i]] = cellsY[2];
    assert isExpandableR[2] == 0;
    
    for i in range(len(R3)):
    	NcellsY[R3[i]] = cellsY[3];
    assert isExpandableR[3] == 0;
    
    # overlap layer - section A
    Ln = 0.8*delta/cellsY[3];   # last cell length
    L0 = Ln*increasedRatio;     # first cell length
    Lt = (Ly1 - delta) - (Ln + L0);     # total overlap distance
   
    for i in range(len(R1)):
    	if isExpandableR[1] == 1:	
    	    NcellsY[R1[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsY[R1[i]] = cellsY[1];
    yRatioR1 = Ln/L0; 
    
    # check if outer-layer is of different refinement between
    # the two halves in section A
    #if cellsY[2] != cellsY[3]:
    L0 = dy1;
    Lt = 0.2*delta - 3.0*dy1;
    Ln = 0.8*delta/cellsY[3];

    for i in range(len(R4)):
    	if isExpandableR[4] == 1:
    	    NcellsY[R4[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsY[R4[i]] = cellsY[4];
    yRatioR4 = Ln/L0; 
   
    
    # mid y-layer in section B 
    for i in range(len(R7)):
        NcellsY[R7[i]] = getNcells(Lt, L0, Ln);
    assert isExpandableR[7] == 0;

    # upper y-layer in section B
    Lt = Ry2*Ly0;
    L0 = dy1;
    Ln = Ry1*Ly0/cellsY[7];
    
    # check code  # # # # # # # # # # # #
    #assert Ln == Ry1*Ly0/NcellsY[R7[0]];#
    # # # # # # # # # # # # # # # # # # #
    
    for i in range(len(R6)):
    	if isExpandableR[6] == 1:
    	    NcellsY[R6[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsY[R6[i]] = cellsY[6]; 
    yRatioR6 = Ln/L0;

    # lowest y-layer in section B
    Lt = Ry0*Ly0;
    L0 = dy1; #Ry1*Ly0/20.0;
    Ln = Ry1*Ly0/cellsY[7];
    
    # check code  # # # # # # # # # # # #
    #assert Ln == Ry1*Ly0/NcellsY[R7[0]];#
    # # # # # # # # # # # # # # # # # # #
    
    for i in range(len(R8)):
    	if isExpandableR[8] == 1:
    	    NcellsY[R8[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsY[R8[i]] = cellsY[8];
    
    yRatioR8 = Ln/L0; 
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # y section (over)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # x section (start)
    for i in range(len(C0)):
    	NcellsX[C0[i]] = cellsX[0];
    assert isExpandableC[0] == 0;
    
    for i in range(len(C1)):
    	NcellsX[C1[i]] = cellsX[1];
    assert isExpandableC[1] == 0;
    
    for i in range(len(C4)):
    	NcellsX[C4[i]] = cellsX[4];
    assert isExpandableC[4] == 0;
    
    for i in range(len(C5)):
    	NcellsX[C5[i]] = cellsX[5];
    assert isExpandableC[5] == 0;
    
    for i in range(len(C6)):
    	NcellsX[C6[i]] = cellsX[6];
    assert isExpandableC[6] == 0;
    
    Lt = Rx2*Lx0;
    L0 = 2*dy1;  
    Ln = Rx1*Lx0/cellsX[1]; 
    
    for i in range(len(C2)):
    	if isExpandableC[2] == 1:
    	    NcellsX[C2[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsX[C2[i]] = cellsX[2];
    xRatioC2 = Ln/L0;

    Lt = Rx3*Lx1;
    L0 = 2*dy1;
    Ln = Rx4*Lx1/cellsX[4];
    
    for i in range(len(C3)):
    	if isExpandableC[3] == 1:
    	    NcellsX[C3[i]] = getNcells(Lt, L0, Ln);
    	else:
    	    NcellsX[C3[i]] = cellsX[3]; 
    xRatioC3 = Ln/L0;
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # x section (over)
    
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # z section (start)
    for i in range(Nblocks):
    	NcellsZ[i] = cellsZ;  
    # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # z section (over)
    
    
    #initialize grading factor for simpleGrading 
    Gx = np.ones(Nblocks);
    Gy = np.ones(Nblocks);
    Gz = np.ones(Nblocks);
    
    
    for i in range(len(C3)):
    	Gx[C3[i]] = xRatioC3;
    for i in range(len(C2)):
    	Gx[C2[i]] = 1.0/xRatioC2;		
    
    for i in range(len(R4)):
    	Gy[R4[i]] = yRatioR4; # CHANGED ! 
    for i in range(len(R1)):
    	Gy[R1[i]] = 1.0/yRatioR1; # CHANGED !
    
    for i in range(len(R8)):
    	Gy[R8[i]] = yRatioR8;
    
    for i in range(len(R6)):
    	Gy[R6[i]] = 1.0/yRatioR6;	

    return block, NcellsX, NcellsY, NcellsZ, Gx, Gy, Gz


# save data to file
def createBlockMeshDict(block, NcellsX, NcellsY, 
                        NcellsZ, Gx, Gy, Gz, 
                        x, y, z, Nblocks, inletFace, 
                        outletFace, stepFace, bottomFace, 
                        topFace, leftFace, rightFace, 
                        scale, fastMerge, OFversion, Nnodes):
 
    # current directory
    baseDir  = os.getcwd();   

    # include directory and file
    thisFile = baseDir + '/system/blockMeshDict'; 

    # write in blockMeshDict 
    scf = open(thisFile, 'w')

    # start printing the layout 
    # OpenFOAM's header template generation
    temp = ('/*--------------------------------*- C++ -*----------------------------------*\\\n'
    '| =========                 |                                                 | \n'
    '| \\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           | \n'
    '|  \\\    /   O peration     | Version:  '+OFversion+'                                 | \n'
    '|   \\\  /    A nd           | Web:      www.OpenFOAM.org                      | \n'
    '|    \\\/     M anipulation  |                                                 | \n'
    '\*---------------------------------------------------------------------------*/ \n' 
    'FoamFile \n' 
    '{\n'
    '\t version\t 2.0;\n'
    '\t format\t\t ascii;\n'
    '\t class\t\t dictionary;\n'
    '\t object\t\t blockMeshDict;\n}\n'
    '// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * // \n\n')
    
    # write to file
    scf.write(temp);
    
    # include directory and file
    thisFile = baseDir + '/meshGenerator/diagram.txt'; 
   
    # insert wirefram diagram for reference purposes !
    f = open(thisFile);
    for line in f.readlines():
    	scf.write(line);
    f.close();	
    scf.write('\n\n');
    # # #
    #                                                   #
    # Block geometry                                    #
    # # # # # # # # # # # # # # # # # # # # # # # # # # #
    
    if fastMerge == 1:
    	scf.write('fastMerge \t\t 1;\n');
    scf.write('convertToMeters \t %f;\n\n'%scale);
   
    temp1 = ('vertices\n'
    '(\n');
    
    temp2 = '';
    for i in range(Nnodes*2):
    	#temp2 += '\t(%2.6f \t %2.6f \t %2.6f) \t // node: %d\n'%(x[i],y[i],z[i],i);
    	if abs(x[i]) < 1e-10:
    		x[i] = 0.0;
    	if abs(y[i]) < 1e-10:
    		y[i] = 0.0;	
    	if abs(z[i]) < 1e-10:
    		z[i] = 0.0;
    	temp2 += '\t(%12.8f \t %12.8f \t %12.8f) \t // node: %d\n'%(x[i],y[i],z[i],i);

    temp3 = (');\n\n');
    
    verticesLayout = temp1 + temp2 + temp3;
    
    scf.write(verticesLayout);
    
    temp1 = ('blocks\n'
    '(\n');
    
    temp2 = '';
    for i in range(Nblocks):
    	temp2 += '\thex (%3d %3d %3d %3d %3d %3d %3d %3d) \t(%3d %3d %3d) \tSimpleGrading\t (%e\t%e\t%e) // b# %d\n'%(
    	block[i, 3],block[i, 2],block[i, 1],block[i, 0],block[i, 7],block[i, 6],block[i, 5],
    	block[i, 4],NcellsX[i],NcellsY[i],NcellsZ[i],Gx[i],Gy[i],Gz[i],i+1);
    
    
    
    temp3 = (');\n\n');
    
    blocksLayout = temp1 + temp2 + temp3;
    
    scf.write(blocksLayout);
    
    edgesLayout = ('edges\n'
    '(\n'
    ');\n\n');
    
    scf.write(edgesLayout);
    
    # boundaries ................................. start !
    temp0 = ('boundary\n'
    '(\n');
    
    temp1 = ('\tinlet\n'
    '\t{\n'
    '\t\ttype \t patch;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    # inlet faces
    for i in range(len(inletFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(inletFace[i,0],inletFace[i,1],inletFace[i,2],inletFace[i,3]);
    
    temp3 = ('\t\t);\n'
    '\t}\n');
    
    inletLayout = temp0 + temp1 + temp2 + temp3;
    scf.write(inletLayout);
    
    # outlet faces
    temp1 = ('\toutlet\n'
    '\t{\n'
    '\t\ttype \t patch;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(outletFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(outletFace[i,0],outletFace[i,1],outletFace[i,2],outletFace[i,3]);
    
    outletLayout = temp1 + temp2 + temp3;
    
    scf.write(outletLayout);
    
    # step faces
    temp1 = ('\tstep\n'
    '\t{\n'
    '\t\ttype \t wall;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(stepFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(stepFace[i,0],stepFace[i,1],stepFace[i,2],stepFace[i,3]);
    
    stepLayout = temp1 + temp2 + temp3;
    
    scf.write(stepLayout);
    
    # bottom faces
    temp1 = ('\tbottom\n'
    '\t{\n'
    '\t\ttype \t wall;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(bottomFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(bottomFace[i,0],bottomFace[i,1],bottomFace[i,2],bottomFace[i,3]);
    
    bottomLayout = temp1 + temp2 + temp3;
    
    scf.write(bottomLayout);
    
    # top faces 
    temp1 = ('\ttop\n'
    '\t{\n'
    '\t\ttype \t symmetry;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(topFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(topFace[i,0],topFace[i,1],topFace[i,2],topFace[i,3]);
    
    topLayout = temp1 + temp2 + temp3;
    
    scf.write(topLayout);
    
    # left faces
    temp1 = ('\tleft\n'
    '\t{\n'
    '\t\ttype \t cyclic;\n'
    '\t\tneighbourPatch \tright;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(leftFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(leftFace[i,0],leftFace[i,1],leftFace[i,2],leftFace[i,3]);
    
    leftLayout = temp1 + temp2 + temp3;
    
    scf.write(leftLayout);
    
    # right faces
    temp1 = ('\tright\n'
    '\t{\n'
    '\t\ttype \t cyclic;\n'
    '\t\tneighbourPatch \tleft;\n'
    '\t\tfaces\n'
    '\t\t(\n');
    
    temp2 = '';
    for i in range(len(rightFace)):
    	temp2 += '\t\t\t(%3d %3d %3d %3d)\n'%(rightFace[i,0],rightFace[i,1],rightFace[i,2],rightFace[i,3]);
    
    rightLayout = temp1 + temp2 + temp3;
    
    scf.write(rightLayout);
    
    scf.write(');\n\n');
    
    # mergePatchPairs ...
    temp1 = ('mergePatchPairs\n'
    '(\n'
    ');\n');
    
    scf.write(temp1);
    
    # close file
    scf.close();



