# # #
# 
# This script is made compatible with the 
# meshGenerator script.                     #
#                                           #
# # # # # # # # # # # # # # # # # # # # # # #

import os
import sys
import utilities as util


# this should return the directory of the case containing the simulation files (system, contstant, ... etc.)
baseDir = os.path.abspath('..');

# get x/h ratio for output
Ninput = len(sys.argv) - 1;
argsIn = sys.argv;
argsIn.pop(0);

# saved time directory after applying averageAlongAxis
timeDir = '135/';

# reference pressure location (for Cp)
Xr_P0 = -5.0;

# mean inlet velocity 
U0 = 2.0294975;

# kinematic viscosity used in the BFS
nu = 2e-4;

# does the model include an SGS-model, nut? 
isSGSmodel = 0;

# extract data after applying averageAlongAxis utility
path2File = baseDir + '/averageAlongAxis/' + timeDir;

if isSGSmodel:
    dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, dataNut, h = util.getCoords(path2File, isSGSmodel);
else:
    dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, h = util.getCoords(path2File, isSGSmodel);


# extract needed profile
Xr = argsIn; # expansion ratios 

# arrange data and return profiles in separated files
if isSGSmodel:
    util.arrangeData(baseDir, Xr, dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, dataNut, h, isSGSmodel);
else:
    dataNut = 0;
    util.arrangeData(baseDir, Xr, dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, dataNut, h, isSGSmodel);

# compute and save all local wall parameters
util.getAllWallParams(baseDir, dataU, dataP, h, U0, nu, Xr_P0);


# TODO: 
# print stats.info file with value of nu, Xr_P0, U0... etc.

