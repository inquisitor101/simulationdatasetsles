import os
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps
from scipy.integrate import quad



def extractData(path2File, fileName):
    data = [];
    idx  = 1;
    
    updatedPath = path2File + fileName;
    with open(updatedPath) as f:
        for line in f:
            if idx > 1:
                temp = line.split();
                data.append(temp);
            idx += 1;
    
    # N = Nx-by-Ny
    N = len(data);

    # data has format: (x,y,z,u/v/w/uu/vv/ww/uv/uw/vw)
    updatedData = np.zeros((N, 4));

    for i in range(N):
        updatedData[i, 0] = float(data[i][1]);
        updatedData[i, 1] = float(data[i][2]);
        updatedData[i, 2] = float(data[i][3]);
        updatedData[i, 3] = float(data[i][4]);

    return updatedData


# computes the step-height distance
def getStepHeight(yCoords):
    # according to the meshGenerator script, 
    # the h is the max(negative(y))
    h = min(yCoords);
    
    return abs(h)

# function to get the (x,y,z) coordinates of the averaged datai via averageAlongAxis utility
def getCoords(path2File, isSGSmodel):

    dataU  = extractData(path2File, 'UMean_X');
    dataV  = extractData(path2File, 'UMean_Y'); 
    dataW  = extractData(path2File, 'UMean_Z'); 
    dataP  = extractData(path2File, 'pMean');
    dataUU = extractData(path2File, 'UPrime2Mean_XX');
    dataVV = extractData(path2File, 'UPrime2Mean_YY');
    dataWW = extractData(path2File, 'UPrime2Mean_ZZ');
    dataUV = extractData(path2File, 'UPrime2Mean_XY');
    dataUW = extractData(path2File, 'UPrime2Mean_XZ');
    dataVW = extractData(path2File, 'UPrime2Mean_YZ');

    h = getStepHeight(dataU[:, 1]);

    if isSGSmodel:
        dataNut = extractData(path2File, 'nutMean');
        return dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, dataNut, h
    else: 
        return dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, h


# arrange and return profile according to imput x/h 
def getProfile(data, targetRatio, h, isPressure):
    
    # decompose data into meaningful parameters
    xCoords = data[:, 0];
    yCoords = data[:, 1];
    params  = data[:, 3];
    # compute actual position
    xTarget = targetRatio*h;
    assert(xTarget >= min(xCoords) and xTarget <= max(xCoords)); 
    # get unique x values
    uniqueXpos, idxListX = np.unique(xCoords, return_index=True); 


    # index corresponding to target ratio x/h
    idx = findNearest(uniqueXpos, xTarget);
    
    # get profile given idx of target ratio x/h
    idxProfile = np.where(xCoords == uniqueXpos[idx]);

    yProfile     = yCoords[idxProfile];
    paramProfile = params[idxProfile];
    xRatio       = uniqueXpos[idx]/h;
    
    # re-arrange data into ascending wall-normal order
    null_, sortedIdx = np.unique(yProfile, return_index=True);

    # # # # # #
    # Address interpolation for case: Xr = 0
    # # # # # #
    if xTarget == 0:
        idxBefore = idx - 1;
        idxAfter  = idx + 1;
        idxProfileB = np.where(xCoords == uniqueXpos[idxBefore]);
        idxProfileA = np.where(xCoords == uniqueXpos[idxAfter]);
        yProfileB = yCoords[idxProfileB];
        yProfileA = yCoords[idxProfileA];
        paramProfileB = params[idxProfileB];
        paramProfileA = params[idxProfileA];
    


        # re-arrange data into ascending wall-normal order
        null_, sortedIdxB = np.unique(yProfileB, return_index=True);
        null_, sortedIdxA = np.unique(yProfileA, return_index=True);

        yProfile0, paramProfile0 = interpolateStepData(yProfileA[sortedIdxA], uniqueXpos[idxAfter], paramProfileA[sortedIdxA], 
                            yProfileB[sortedIdxB], uniqueXpos[idxBefore], paramProfileB[sortedIdxB], 
                            yProfile[sortedIdx], uniqueXpos[idx], paramProfile[sortedIdx], h, isPressure);
    
        return yProfile0, paramProfile0, xRatio
    else:
        return yProfile[sortedIdx], paramProfile[sortedIdx], xRatio


# interpolate at the center (step)
def interpolateStepData(yProfA, dxA, paramProfA, 
                        yProfB, dxB, paramProfB, 
                        yProf,  Xr,  paramProf, 
                        h, isPressure):

    # total cells after step
    Nafter  = len(yProfA);
    # total cells before step
    Nbefore = len(yProfB);
    # total cells at the step boundary
    Nstep   = len(yProf);

    # assert everything is in solid condition
    assert(Nstep == (Nafter - Nbefore));

    # change from negative distance to positive
    dxB = abs(dxB);

    # assert cell width is the same (as should be specified 
    # in the meshGenerator script)
    #assert(dxB == dxA);

    # interpolate linearly between profiles before and after the step
    tempY = np.zeros(Nafter);
    tempP = np.zeros(Nafter);
    
    # ratio for interpolation
    dxRatio = dxB/(dxB + dxA);
    
    # bottom step flow
    for i in range(Nstep+1):
        tempY[i] = yProfA[i];
    if isPressure == 1:
        for i in range(Nstep+1):
            tempP[i] = paramProfA[i];
    else:
        for i in range(Nstep+1):   
            tempP[i] = 0.0;

    # shear-layer flow
    for i in range(Nstep+1, Nafter):
        tempY[i] = yProfA[i];
        tempP[i] = paramProfB[i-Nstep] + (paramProfA[i] - paramProfB[i-Nstep])*dxRatio;

    return tempY, tempP


# find nearest expansion ratio from mesh
def findNearest(array, value):
    # get index of nearest value
    idx = (np.abs(array-value)).argmin();
    return idx


# save data to file
def saveData(baseFile, fileName, profiles, Xr, isSGSmodel):
   
    directory = baseFile + '/profiles';
    # create directory for saving profiles
    if not os.path.exists(directory):
        os.makedirs(directory);

    # profiles format: (y, U, V, W, p, UU, VV, WW, UV, UW, VW, nut?)
    

    # initialize file to write in
    path2File = directory + '/' + fileName;   
    scf = open(path2File, 'w');

    temp  = '# # # # # # #\n';
    temp += '#\n';
    temp += '#  Xr: %10.6f \n'%(Xr);
    temp += '#\n';

    if isSGSmodel:
        temp += '#     y\t<U>\t<V>\t<W>\t<p>\t<UU>\t<VV>\t<WW>\t<UV>\t<UW>\t<VW>\t<nut>\n';
        temp += '#######################################################################################################################################################################\n';
    
        for i in range(len(profiles[0])):
            temp += '%e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e\n'%(profiles[0, i], profiles[1, i], profiles[2, i], profiles[3, i], profiles[4, i], profiles[5, i], profiles[6, i], profiles[7, i], profiles[8, i], profiles[9, i], profiles[10, i], profiles[11, i]);



    else:
        temp += '#     y\t<U>\t<V>\t<W>\t<p>\t<UU>\t<VV>\t<WW>\t<UV>\t<UW>\t<VW>\n';
        temp += '#######################################################################################################################################################################\n';
    
        for i in range(len(profiles[0])):
            temp += '%e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e\n'%(profiles[0, i], profiles[1, i], profiles[2, i], profiles[3, i], profiles[4, i], profiles[5, i], profiles[6, i], profiles[7, i], profiles[8, i], profiles[9, i], profiles[10, i]);

    # write to file
    scf.write(temp);


# plot data?
def visualizeData(someCoords, someData, isOn):
    if isOn:
        plt.plot(someCoords, someData);
        plt.grid(True);
        plt.show();
    else:
        pass

# arrange data 
def arrangeData(baseFile, Xr, dataU, dataV, dataW, dataP, dataUU, dataVV, dataWW, dataUV, dataUW, dataVW, dataNut, h, isSGSmodel):


    # total number of input streamwise ratios: Xr
    N = len(Xr);

    for i in range(N):
        yProfile1,  uProfile,  xRatio1 =  getProfile(dataU,  float(Xr[i]), h, 0);       
        yProfile2,  vProfile,  xRatio2 =  getProfile(dataV,  float(Xr[i]), h, 0);
        yProfile3,  wProfile,  xRatio3 =  getProfile(dataW,  float(Xr[i]), h, 0);
        yProfile4,  pProfile,  xRatio4 =  getProfile(dataP,  float(Xr[i]), h, 1);
        yProfile5,  uuProfile, xRatio5 =  getProfile(dataUU, float(Xr[i]), h, 0); 
        yProfile6,  vvProfile, xRatio6 =  getProfile(dataVV, float(Xr[i]), h, 0);        
        yProfile7,  wwProfile, xRatio7 =  getProfile(dataWW, float(Xr[i]), h, 0); 
        yProfile8,  uvProfile, xRatio8 =  getProfile(dataUV, float(Xr[i]), h, 0); 
        yProfile9,  uwProfile, xRatio9 =  getProfile(dataUW, float(Xr[i]), h, 0); 
        yProfile10, vwProfile, xRatio10 = getProfile(dataVW, float(Xr[i]), h, 0); 
        
        if isSGSmodel:
            yProfile11, nutProfile, xRatio11 = getProfile(dataNut, float(Xr[i]), h, 0); 
        
        assert(xRatio1 == xRatio2);
        np.testing.assert_almost_equal(yProfile1, yProfile2);
        assert(xRatio2 == xRatio3);
        np.testing.assert_almost_equal(yProfile2, yProfile3);
        assert(xRatio3 == xRatio4);
        np.testing.assert_almost_equal(yProfile3, yProfile4);
        assert(xRatio4 == xRatio5);
        np.testing.assert_almost_equal(yProfile4, yProfile5);
        assert(xRatio5 == xRatio6);
        np.testing.assert_almost_equal(yProfile5, yProfile6);
        assert(xRatio6 == xRatio7);
        np.testing.assert_almost_equal(yProfile6, yProfile7);
        assert(xRatio7 == xRatio8);
        np.testing.assert_almost_equal(yProfile7, yProfile8);
        assert(xRatio8 == xRatio9);
        np.testing.assert_almost_equal(yProfile8, yProfile9);
        assert(xRatio9 == xRatio10);
        np.testing.assert_almost_equal(yProfile9, yProfile10);
        if isSGSmodel:
            assert(xRatio10 == xRatio11);
            np.testing.assert_almost_equal(yProfile10, yProfile11);


        
        # convert data into single matrix
        if isSGSmodel:
            data = np.vstack((yProfile1, 
                          uProfile,  vProfile,  wProfile,
                          pProfile,
                          uuProfile, vvProfile, wwProfile,
                          uvProfile, uwProfile, vwProfile,
                          nutProfile));

        else:
            data = np.vstack((yProfile1, 
                          uProfile,  vProfile,  wProfile,
                          pProfile,
                          uuProfile, vvProfile, wwProfile,
                          uvProfile, uwProfile, vwProfile));

        # file name to be saved in profiles/
        fileName = str(Xr[i]) + '.xy';
        saveData(baseFile, fileName, data, xRatio1, isSGSmodel);



# TODO:
# get friction velocity and shear wall stress 
def getAllWallParams(baseFile, dataU, dataP, h, U0, nu, Xr_P0):

    # decompose data into meaningful parameters
    xCoords  = dataU[:, 0];
    # get unique x values
    x, null_ = np.unique(xCoords, return_index=True); 
    
    # total cells
    N = len(x);
    
    # get x/h ratio of x-coordinates 
    # (this facilitates the use of pre-built functions)
    Xr = np.zeros(N);
    Xr = np.dot(x, 1.0/h);

    # initialize skin-friction coefficient
    Cf = np.zeros(N);

    # initialize step-wall pressure coefficient
    Cp = np.zeros(N);

    # initialize friction velocity near wall
    uTau = np.zeros(N);

    # initialize first y-cell width w.r.t. bottom wall
    dyPlus1 = np.zeros(N);

    # initialize last y-cell width inner-scaled
    dyPlusN = np.zeros(N);

    # initialize last y-cell width outer-scaled with delta99
    dyDeltaN = np.zeros(N);

    # initialize last y-cell width outer-scaled with theta
    dyThetaN = np.zeros(N);

    # initialize x-cell width w.r.t. bottom wall
    dxPlus = np.zeros(N);

    # initialize local ReTau w.r.t. bottom wall
    ReTau = np.zeros(N);

    # compute x-cell width
    trueX = np.zeros(N);
    cellX = np.zeros(N);
    trueX[0] = -10*h;
    for i in range(len(x)-1):
        trueX[i+1] = trueX[i] + 2*(x[i] - trueX[i]);
        cellX[i] = 2*(x[i] - trueX[i]);
    cellX[i+1] = 2*(x[i+1] - trueX[i+1]);


    # specify reference pressure according to location: Xr_P0
    # NB: in OpenFOAM, p = p/\rho 
    # TODO: needs validation !
    # Xr_Pref is the x/h location of P0 nearest to targetet Xr_P0
    null_, temp, Xr_Pref = getProfile(dataP, float(Xr_P0), h, 1);
    P0 = temp[0]; # take wall-contact (boundary) value

    # get all profiles
    for i in range(N):
        y,       u, xRatio1 = getProfile(dataU, float(Xr[i]), h, 0);       
        null_,   p, xRatio2 = getProfile(dataP, float(Xr[i]), h, 1);

        # assert same ratio exist
        np.testing.assert_almost_equal(xRatio1, Xr[i]);
        np.testing.assert_almost_equal(xRatio2, Xr[i]);

        # get skin-friction coefficient
        Cf[i] = 2.0*nu*(u[1])/(y[1]-y[0])/U0**2;
        
        # TODO: needs validation !
        # get step-wall pressure coefficient
        #   density cancels out since it's inclusive in the pressure terms
        Cp[i] = 2.0*(p[0] - P0)/U0**2; 

        # get friction velocity
        dUdy = math.fabs((u[1] - u[0])/(y[1] - y[0]));
        uTau[i] = math.sqrt(nu*dUdy); 
        
        # get first y-cell width 
        dyPlus1[i] = (y[1] - y[0])*uTau[i]/nu;

        # get x-cell width 
        dxPlus[i] = cellX[i]*uTau[i]/nu;

        # get delta99 of TBL 
        delta99 = 0.0
        for j in range(len(y)):
            if u[j] > 0.99*u[-1]:
                delta99 = y[j-1];
                break
        assert(delta99 > 0.0);
        # get largest y-cell still inside the TBL
        dy = np.diff(y[0:j+1]);
        dyMax = np.max(dy);

        # get last y-cell outer-scaled with delta99
        dyDeltaN[i] = (dyMax)/delta99; 

        # get last y-cell width inner-scaled
        dyPlusN[i] = (dyMax)*uTau[i]/nu;

        # compute momentum thickness 
        theta = simps(u/u[-1]*(1-u/u[-1]), x=y);

        # get last y-cell outer-scaled with theta
        dyThetaN[i] = (dyMax)/theta;

        # compute local friction Re
        ReTau[i] = uTau[i]*delta99/nu;

        if i == 0:
            # compute inflow ReTheta: momentum thicknes Reynolds number
            ReTheta = U0*theta/nu;

    # write data to file 
    saveCoefficients(baseFile, Cf, Cp, uTau, dyPlus1, dyPlusN, Xr, ReTheta, dyDeltaN, dyThetaN, dxPlus, ReTau);


# TODO: save Cf and Cp data to file 
def saveCoefficients(baseFile, Cf, Cp, uTau, dyPlus1, dyPlusN, Xr, ReTheta, dyDeltaN, dyThetaN, dxPlus, ReTau):
    
    directory = baseFile + '/profiles';
    # create directory for saving profiles
    if not os.path.exists(directory):
        os.makedirs(directory);

    fileName = 'wallParams.xy'; 

    # initialize file to write in
    path2File = directory + '/' + fileName;   
    scf = open(path2File, 'w');

    temp  = '# # # # # # #\n';
    temp += '# ReTheta = %e\n'%ReTheta;
    temp += '#\n';
    temp += '#  coeffients of friction, pressure, local friction velocity, first and last wall-normal cell widths\n';
    temp += '#\n';
    temp += '#     x/h\tCf\tCp\tuTau\tdyPlus1\tdyPlusN\tdyDeltaN\tdyThetaN\tdxPlus\tReTau\n';
    temp += '#########################################################################################################\n';
    
    for i in range(len(Xr)):
        temp += '%e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e\n'%(Xr[i], Cf[i], Cp[i], uTau[i], dyPlus1[i], dyPlusN[i], dyDeltaN[i], dyThetaN[i], dxPlus[i], ReTau[i]);

    # write to file
    scf.write(temp);


